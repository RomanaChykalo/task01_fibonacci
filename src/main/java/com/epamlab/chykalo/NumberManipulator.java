/*
 * Package for training.
 */
package com.epamlab.chykalo;

/**
 * Class NumberManipulator consists of methods that operate on numbers, which are in specific interval.
 *
 * @author Romana Chykalo
 * @since 2019-01-22
 */

public class NumberManipulator {
    /**
     * Method printOddNumbersMinMax prints odd numbers from start to the end of interval;
     *
     * @param minNumber start of number interval
     * @param maxNumber end of number interval
     */
    public void printOddNumbersMinMax(int minNumber, int maxNumber) {
        for (int i = minNumber; i <= maxNumber; i++) {
            if (i % 2 == 1) {
                System.out.print(i + " ");
            }
        }
    }

    /**
     * Method printOddNumbersMaxMin prints odd numbers from the end to beginning of interval;
     *
     * @param maxNumber start of number interval
     * @param minNumber end of number interval
     */
    public void printOddNumbersMaxMin(int maxNumber, int minNumber) {
        for (int i = maxNumber; i >= minNumber; i--) {
            if (i % 2 == 1) {
                System.out.print(i + " ");
            }
        }
    }

    /**
     * Method countSumOddAndEvenNumbers calculates and prints sum of odd and even numbers;
     *
     * @param minNumber start of number interval
     * @param maxNumber end of number interval
     */
    public void countSumOddAndEvenNumbers(int minNumber, int maxNumber) {
        int sumOdd = 0;
        int sumEven = 0;
        for (int i = minNumber; i <= maxNumber; i++) {
            if (i % 2 == 1) {
                sumOdd += i;
            } else {
                sumEven += i;
            }
        }
        System.out.println("In range from " + minNumber + " to " + maxNumber + " sum of odd numbers is " + sumOdd);
        System.out.println("In range from " + minNumber + " to " + maxNumber + " sum of even numbers is " + sumEven);
    }

    /**
     * Method findPercentageFibonacciSequence calculates and prints percentage of odd and even Fibonacci numbers;
     *
     * @param amountOfElements it is size of set, amount of elements in Fibonacci sequence.
     */

    public void findPercentageFibonacciSequence(int amountOfElements) {
        int[] array = new int[amountOfElements];
        array[0] = 1;
        array[1] = 1;
        int amountOfOddNumbers = 2;
        int amountOfEvenNumbers = 0;
        for (int index = 2; index < amountOfElements; index++) {
            array[index] = array[index - 1] + array[index - 2];
            if (array[index] % 2 == 1) {
                amountOfOddNumbers += 1;
            } else {
                amountOfEvenNumbers += 1;
            }
        }
        double oddPart = (double) amountOfOddNumbers / amountOfElements;
        double oddPercent = oddPart * 100;
        double evenPart = (double) amountOfEvenNumbers / amountOfElements;
        double evenPercent = evenPart * 100;
        System.out.println("Percentage of odd numbers is: " + oddPercent + "%");
        System.out.println("Percentage of even numbers is: " + evenPercent + "%");
    }

    /**
     * Main method is the entry point of a Java application
     * It makes use of all methods.
     *
     * @param args Unused.
     */
    public static void main(String[] args) {
        NumberManipulator manipulator = new NumberManipulator();
        manipulator.printOddNumbersMinMax(2, 19);
        manipulator.printOddNumbersMaxMin(20, 1);
        manipulator.countSumOddAndEvenNumbers(3, 90);
        manipulator.findPercentageFibonacciSequence(9);

    }
}



